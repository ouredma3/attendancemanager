<?php

namespace App\Traits;

use Illuminate\Http\Response as Response;

trait ApiResponser
{
    /**
     * Building success response
     * @param $data
     * @param int $code
     * @return Response
     */
    public function successResponse($data, $code = Response::HTTP_OK) : Response
    {
        return response($data,$code);
    }


    public function errorResponse($message, $code) : Response
    {
        return response()->json(['error' => $message, 'code' => $code], $code);
    }

}