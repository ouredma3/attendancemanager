<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 26/11/2019
 * Time: 19:37
 */

namespace App\Http\Controllers;


use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Attendance;
use App\Traits\ApiResponser;

class AttendanceController extends Controller
{
    use ApiResponser;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Return full list of attendance for user
     */
    public function getAttendance($user)
    {
        //$attendance = Attendance::where('user_id', $user)->whereNotNull('to')->orderBy('from', 'desc')->paginate(2);
        $attendance = Attendance::where('user_id', $user)->whereNotNull('to')->orderBy('from', 'desc')->get()->toArray();
        return response()->json($attendance);
    }

    public function getActiveAttendance($user)
    {
        $attendance = Attendance::where('user_id', $user)->where('to', null)->first();
        return response()->json($attendance);
    }

    public function startAttendance(Request $request, $user)
    {


        try {
            //validate incoming request
            $this->validate($request, [
                'from' => 'required|date_format:H:i:s',
                'from_date' => 'required|date_format:Y-m-d',
            ]);
            $active_attendance = Attendance::where('user_id', $user)->where('to', null)->first();
            if ($active_attendance != null) {
                return response()->json(['message' => 'There already is active tracking'], 409);
            }
            $attendance = new Attendance();
            $attendance->from = $request->input('from');
            $attendance->from_date = $request->input('from_date');
            $attendance->user_id = $user;
            if ($request->input('description') != null) {
                $attendance->description = $request->input('description');
            }

            if ($request->input('to') != null) {
                $this->validate($request, [
                    'to' => 'date_format:H:i:s',
                    'to_date' => 'date_format:Y-m-d',
                    'task_id' => 'required|integer',
                ]);
                $attendance->to = $request->input('to');
                $attendance->to_date = $request->input('to_date');
                $attendance->task_id = $request->input('task_id');

                $attendance->time = Carbon::createFromTimestamp($attendance->from_date . $attendance->from)->diffInSeconds(Carbon::createFromTimestamp($attendance->to_date . $attendance->to));

            }
            if( $request->input('task_id') != ''){
                $this->validate($request, [
                    'task_id' => 'integer',
                ]);
                $attendance->task_id = $request->input('task_id');
            }
            $attendance->save();
            //return successful response
            return response()->json(['attendance' => $attendance, 'message' => 'CREATED'], 201);

        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => 'Attendance Creation Failed!'], 409);
        }
    }

    public function endAttendance(Request $request, $user)
    {
        try {

            $this->validate($request, [
                'to' => 'required|date_format:H:i:s',
                'to_date' => 'required|date_format:Y-m-d',
                'task_id' => 'required|integer',
            ]);

            $attendance = Attendance::where('user_id', $user)->where('to', null)->first();
            if ($attendance == null) {
                return response()->json(['message' => 'No active attendance'], 404);
            }
            if ($request->input('from') != null) {
                $this->validate($request, [
                    'from' => 'required|date_format:H:i:s',
                    'from_date' => 'required|date_format:Y-m-d',
                ]);
                $attendance->from = $request->input('from');
                $attendance->from_date = $request->input('from_date');
            }

            $attendance->to = $request->input('to');
            $attendance->to_date = $request->input('to_date');
            $attendance->task_id = $request->input('task_id');
            if ($request->input('description') != null) {
                $attendance->description = $request->input('description');
            }

            $attendance->time = Carbon::createFromTimestamp(strtotime($attendance->from_date . ' ' . $attendance->from))->diffInSeconds(Carbon::createFromTimestamp(strtotime($attendance->to_date . ' ' . $attendance->to)));
            $attendance->save();
            //return successful response
            return response()->json(['attendance' => $attendance, 'message' => 'CREATED'], 201);

        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => 'Attendance Creation Failed!'], 409);
        }
    }

    public function editAttendance(Request $request, $id)
    {
        try {

            $this->validate($request, [
                'from' => 'required|date_format:H:i:s',
                'from_date' => 'required|date_format:Y-m-d',
                'to' => 'required|date_format:H:i:s',
                'to_date' => 'required|date_format:Y-m-d',
                'task_id' => 'required|integer',
            ]);


            $attendance = Attendance::find($id);

            $attendance->description = $request->input('description');
            $attendance->from = $request->input('from');
            $attendance->from_date = $request->input('from_date');
            $attendance->to = $request->input('to');
            $attendance->to_date = $request->input('to_date');
            $attendance->task_id = $request->input('task_id');
            $attendance->time = Carbon::createFromTimestamp(strtotime($attendance->from_date . ' ' . $attendance->from))->diffInSeconds(Carbon::createFromTimestamp(strtotime($attendance->to_date . ' ' . $attendance->to)));

            $attendance->save();

            //return successful response
            return response()->json(['attendance' => $attendance, 'message' => 'CREATED'], 201);

        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => 'Attendance edit failed!'], 409);
        }
    }

    /**
     * @param $id user ID
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeAllAttendance($id){
        Attendance::where('user_id', $id)->delete();
        return response()->json(['message' => 'All users attendance successfully deleted'], 204);
    }

    public function removeAttendance($id){
        Attendance::find($id)->delete();
        return response()->json(['message' => 'Attendance successfully deleted'], 204);
    }

}