<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class Attendance extends Model
{

    protected $fillable = [
        'from', 'from_date', 'to', 'to_date', 'user_id', 'task_id', 'description', 'time'
    ];

}
