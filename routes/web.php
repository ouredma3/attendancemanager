<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/attendances/{user}', 'AttendanceController@getAttendance');

$router->get('/attendances/{user}/active', 'AttendanceController@getActiveAttendance');

$router->post('/attendances/{id}/start', 'AttendanceController@startAttendance');

$router->post('/attendances/{id}/end', 'AttendanceController@endAttendance');

$router->put('/attendances/{id}', 'AttendanceController@editAttendance');

$router->delete('/attendances/{id}/all', 'AttendanceController@removeAllAttendance');

$router->delete('/attendances/{id}', 'AttendanceController@removeAttendance');